package com.redturtle.ca7s.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.redturtle.ca7s.R

/**
 * Created by Red Turtle-04 on 28-02-2018.
 */
class album_frg_adpt :RecyclerView.Adapter<album_frg_adpt.MyViewHolder> {

    var album  = arrayOf<String>()
    var context: Context

    constructor(album: Array<String>, context: Context) : super() {
        this.album = album
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int):MyViewHolder {

        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.row_album_adpt,parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        holder!!.textalbum.text = album[position]
    }

    override fun getItemCount(): Int {
       return album.size
    }

    inner class MyViewHolder(itemview : View) : RecyclerView.ViewHolder(itemview)
    {
        var textalbum:TextView = itemview.findViewById(R.id.albumtitleiv)

    }
}