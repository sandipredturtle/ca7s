package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.redturtle.ca7s.R
import kotlinx.android.synthetic.main.include_bottom_tab.*

class SearchActivty : AppCompatActivity() {
    private lateinit var ardiogroup: RadioGroup
    private lateinit var rbfavourite : RadioButton
    private lateinit var rbmusic : RadioButton
    private lateinit var rbbroadcast : RadioButton
    private lateinit var rbsearch : RadioButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_activty)
        ardiogroup=findViewById(R.id.radiogrp)

        ardiogroup.check(R.id.rbsearch)
        ardiogroup.setOnCheckedChangeListener(object:RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {

                rbfavourite=group.findViewById(R.id.rbfav)
                rbmusic=group.findViewById(R.id.rbmusic)
                rbbroadcast=group.findViewById(R.id.rbbroad)
                rbsearch=group.findViewById(R.id.rbsearch)
                when(ardiogroup.checkedRadioButtonId) {
                    R.id.rbfav -> {
                        startActivity(Intent(applicationContext,Favourites::class.java))
                        rbfavourite.isChecked=false
                        rbsearch.isChecked=true
                    }
                    R.id.rbmusic ->{
                        startActivity(Intent(applicationContext,MyMusicActivty::class.java))
                        rbmusic.isChecked = false
                        rbsearch.isChecked = true
                    }
                    R.id.rbbroad -> {
                        startActivity(Intent(applicationContext,BroadcastActivity::class.java))
                        rbbroad.isChecked = false
                        rbsearch.isChecked = true

                    }
                }
            }
        })
    }

    override fun onBackPressed() {

        super.onBackPressed()
    }
}

