package com.redturtle.ca7s.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.redturtle.ca7s.R

class MusicPlayerActivity : AppCompatActivity() {

    private lateinit var textmusic:TextView
    private lateinit var textdescription:TextView
    private lateinit var mpimage:ImageView
    private lateinit var mpdarrow:ImageView
    private lateinit var relativel:RelativeLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player)
        textmusic=findViewById(R.id.mpmusictv)
        textdescription=findViewById(R.id.mpsubtitletv)
        mpimage=findViewById(R.id.mainmusiciv)
        mpdarrow=findViewById(R.id.mpdwoniv)
        relativel=findViewById(R.id.mainmusicrl)

        //getmusic
        textmusic.text=intent.getStringExtra("title")
        textdescription.text=intent.getStringExtra("subtitle")

        mpimage.setOnClickListener {
            mpimage.visibility=View.GONE
            relativel.visibility=View.VISIBLE
        }
        mpdarrow.setOnClickListener {
            mpimage.visibility=View.VISIBLE
            relativel.visibility=View.GONE
        }
    }
}
