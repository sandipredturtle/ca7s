package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import com.redturtle.ca7s.R




class BroadcastActivity : AppCompatActivity() {

    private lateinit var ardiogroup: RadioGroup
    private lateinit var rbfavourite :RadioButton
    private lateinit var rbmusic :RadioButton
    private lateinit var rbbroadcast :RadioButton
    private lateinit var rbsearch :RadioButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast)
        ardiogroup=findViewById(R.id.radiogrp)


        ardiogroup.check(R.id.rbbroad)

        ardiogroup.setOnCheckedChangeListener(object:RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {

                rbfavourite=group.findViewById(R.id.rbfav)
                rbmusic=group.findViewById(R.id.rbmusic)
                rbbroadcast=group.findViewById(R.id.rbbroad)
                rbsearch=group.findViewById(R.id.rbsearch)



                when(ardiogroup.checkedRadioButtonId) {
                    R.id.rbfav -> {
                        startActivity(Intent(applicationContext,Favourites::class.java))
                        rbfavourite.isChecked=false
                        rbbroadcast.isChecked = true
                    }
                    R.id.rbmusic -> {
                        startActivity(Intent(applicationContext,MyMusicActivty::class.java))
                        rbmusic.isChecked = false
                        rbbroadcast.isChecked=true
                    }
                    R.id.rbsearch -> {
                        startActivity(Intent(applicationContext,SearchActivty::class.java))
                        rbsearch.isChecked = false
                        rbbroadcast.isChecked = true
                    }
                }
            }
        })




    }
}
