package com.redturtle.ca7s.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.redturtle.ca7s.R
import com.redturtle.ca7s.adapter.MyMusic_List_Adpt
import kotlinx.android.synthetic.main.include_bottom_tab.*

class MyMusicActivty : AppCompatActivity() {

    private lateinit var ardiogroup: RadioGroup
    private lateinit var rbfavourite : RadioButton
    private lateinit var rbmusic : RadioButton
    private lateinit var rbbroadcast : RadioButton
    private lateinit var rbsearch : RadioButton
    private lateinit var context: Context
    private lateinit var layout:LinearLayoutManager
    private lateinit var recycler:RecyclerView
    private lateinit var  filter:ImageView
    private var arr = arrayOf<String>("Aorem ipsum","Alorem ipsum","Ason ipsum","Atrorem  ipsum","Bsdefh ipsum","Baalenb ipsum","Csrm ipsum","Cftorem ipsum","Gonorem ipsum","Ykrrem ipsum")
    private var arrs = arrayOf<String>("lorem ipsum","Alom ipsum","dummy ipsum","despers  ipsum","chiph ipsum","thrills ipsum","arget ipsum"," cget ipsum","nnon ipsum","hyikem ipsum")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_music_activty)
        ardiogroup=findViewById(R.id.radiogrp)
        recycler=findViewById(R.id.musicrecycler)
        filter=findViewById(R.id.musicfilter)

        ardiogroup.check(R.id.rbmusic)
        ardiogroup.setOnCheckedChangeListener(object: RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {

                rbfavourite=group.findViewById(R.id.rbfav)
                rbmusic=group.findViewById(R.id.rbmusic)
                rbbroadcast=group.findViewById(R.id.rbbroad)
                rbsearch=group.findViewById(R.id.rbsearch)
                when(ardiogroup.checkedRadioButtonId) {
                    R.id.rbfav ->{
                        startActivity(Intent(applicationContext,Favourites::class.java))
                        rbfavourite.isChecked=false
                        rbmusic.isChecked=true
                    }
                    R.id.rbbroad ->{
                        startActivity(Intent(applicationContext,BroadcastActivity::class.java))
                        rbbroadcast.isChecked=false
                        rbmusic.isChecked=true
                    }
                    R.id.rbsearch ->{
                        startActivity(Intent(applicationContext,SearchActivty::class.java))
                        rbsearch.isChecked=false
                        rbmusic.isChecked=true
                        }
                }
            }
        })
        context=this
        filter.setOnClickListener {

            filtersheet()
        }

        layout = LinearLayoutManager(this)
        recycler.layoutManager=layout
        recycler.setHasFixedSize(true)
        recycler.itemAnimator = DefaultItemAnimator()

        val adpt = MyMusic_List_Adpt(arr,arrs,this)
        recycler.adapter=adpt
    }

    @SuppressLint("ResourceType")
    private fun filtersheet()
    {
        val bottomSheetDialog = BottomSheetDialog(this)
        val parentview = layoutInflater.inflate(R.layout.row_bottomsheet_filter, null)
        bottomSheetDialog.setContentView(parentview)
        val bottomSheetBehavior = BottomSheetBehavior.from(parentview.parent as View)
        bottomSheetDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

//        bottomSheetDialog.window.setFeatureDrawableAlpha(R.layout.row_bottomsheet_filter,0.9F.toInt())
       // bottomSheetDialog.window.findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetBehavior.peekHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300f, resources.displayMetrics).toInt()
        bottomSheetDialog.show()
    }
}
