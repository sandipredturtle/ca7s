package com.redturtle.ca7s.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.redturtle.ca7s.R
import com.redturtle.ca7s.activity.MainPlayActivity
import com.redturtle.ca7s.activity.MusicPlayerActivity
import com.redturtle.ca7s.adapter.album_frg_adpt
import com.redturtle.ca7s.adapter.playlist_frg_adpt


/**
 * A simple [Fragment] subclass.
 */
class PlaylistFragment : Fragment(),playlist_frg_adpt.onclickfragment {


    private lateinit var linear : GridLayoutManager
    private lateinit var recycler: RecyclerView
    private var albumarr = arrayOf("My Songs","Thumbs up","Last added")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_playlist,container,false)
        recycler=view.findViewById(R.id.frgplayrv)
        linear = GridLayoutManager(activity,2)
        recycler.layoutManager = linear
        recycler.setHasFixedSize(true)
        recycler.itemAnimator = DefaultItemAnimator()

        val adpt = playlist_frg_adpt(view.context,albumarr,this)
        recycler.adapter = adpt
        return view
    }
    override fun clickitem(ipo: Int) {
       var intent = Intent(view!!.context,MainPlayActivity::class.java)
        intent.putExtra("arrayname", albumarr[ipo])
        startActivity(intent)
    }
}// Required empty public constructor
