package com.redturtle.ca7s.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.redturtle.ca7s.R
import com.redturtle.ca7s.adapter.album_frg_adpt


/**
 * A simple [Fragment] subclass.
 */
class AlbumFragment : Fragment() {

    private lateinit var linear : GridLayoutManager
    private lateinit var recycler:RecyclerView
    private var albumarr = arrayOf("Album 1","Album 2","Album 3","Album 4","Album 5")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_album,container,false)

        recycler = view.findViewById(R.id.frgalbumrv)

        linear = GridLayoutManager(activity,3)
        recycler.layoutManager = linear
        recycler.setHasFixedSize(true)
        recycler.itemAnimator = DefaultItemAnimator()

        val adpt = album_frg_adpt(albumarr,view.context)
        recycler.adapter = adpt
        return view
    }


}// Required empty public constructor
